package com.javaEnterprize.dz1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ResourceLoaderServiceTest {

    @Test
    void getResourceDataUsingFilePath() throws IOException {
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
        ResourceLoaderService question = (ResourceLoaderService) context.getBean("question");
        String[] expectedResult=new String[]{" What dish is your favorite?",
                " What color is your favorite?",
                " Where do you live?",
                " What profession do you have?",
                " Do you like animals?"
        };

        String[] questions = question.getResourceDataUsingFilePath();
        Assertions.assertArrayEquals(expectedResult,questions);
    }
}