package com.javaEnterprize.dz1;

import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.util.Scanner;

public class MenuService {

    Scanner sc = new Scanner(System.in);
    Form form;
    public void callMenu(ApplicationContext context) throws IOException {
        ResourceLoaderService question = (ResourceLoaderService) context.getBean("question");
        this.form=(Form) context.getBean("form");
        String[] questions = question.getResourceDataUsingFilePath();

        System.out.println(" What is your first name?");
        form.setName(sc.nextLine());
        System.out.println(" What is your last name?");
        form.setLastname(sc.nextLine());
        System.out.println(questions[0]);
        form.setFavoriteDish(sc.nextLine());


        System.out.println(questions[1]);
        form.setFavoriteColor(sc.nextLine());

        System.out.println(questions[2]);
        form.setPlaceOfLiving(sc.nextLine());

        System.out.println(questions[3]);
        form.setProfession(sc.nextLine());

        System.out.println(questions[4]);
        form.setIsLikeAnimals(sc.nextLine());
        System.out.println(form);

    }
    public  void getForm(){
        form.getAllDataForm();

    }
}
