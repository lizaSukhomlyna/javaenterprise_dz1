package com.javaEnterprize.dz1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;


public class MainApp {

    public static void main(String[] args) {
        try {
            ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
            MenuService menu = (MenuService) context.getBean("menu");
            menu.callMenu(context);
            menu.getForm();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }

}
