package com.javaEnterprize.dz1;


import lombok.Setter;

@Setter
public class Form {
    String name;
    String lastname;
    String favoriteDish;
    String favoriteColor;
    String placeOfLiving;
    String profession;
    String isLikeAnimals;

    public void getAllDataForm(){
        System.out.println("________________________");
        System.out.println("Form");
        System.out.println("________________________");
        System.out.printf("Name: %s%n",this.name);
        System.out.printf("LastName: %s%n",this.lastname);
        System.out.printf("FavoriteDish: %s%n",this.favoriteDish);
        System.out.printf("FavoriteColor: %s%n",this.favoriteColor);
        System.out.printf("PlaceOfLiving: %s%n",this.placeOfLiving);
        System.out.printf("Profession: %s%n",this.profession);
        System.out.printf("isLikeAnimals: %s%n",this.isLikeAnimals);
        System.out.println("________________________");

    }
}
