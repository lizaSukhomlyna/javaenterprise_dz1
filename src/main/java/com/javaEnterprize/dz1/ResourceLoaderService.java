package com.javaEnterprize.dz1;
import lombok.Setter;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Setter
public class ResourceLoaderService implements ResourceLoaderAware {

    private ResourceLoader resourceLoader;
    private String location;

    public String[] getResourceDataUsingFilePath() throws IOException {
        Resource resource = resourceLoader.getResource(location);
        InputStream in = resource.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String[] lines=new String[5];
        for (int i = 0; i < lines.length; i++) {
            String line = reader.readLine().split(":")[1];
            if (line == null)
                break;  lines[i]=line;
        } reader.close();
        return lines;
    }


}
